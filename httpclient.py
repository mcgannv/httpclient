"""
- CS2911 - 0NN
- Fall 2019
- Lab 5
- Names:
  - Michael Caballero
  - Vil McGann

A simple HTTP client
"""

# import the "socket" module -- not using "from socket import *" in order to selectively use items with "socket." prefix
import socket

# import the "regular expressions" module
import re


def main():
    """
    Tests the client on a variety of resources
    """
    # get_http_resource("http://192.168.24.129:8080/mm.jpg", 'mm.jpg')
    # # These resource request should result in "Content-Length" data transfer
    # get_http_resource('http://msoe.us/CS/cs1.1chart.png', 'cs1.1chart.png')
    #
    # # this resource request should result in "chunked" data transfer
    # get_http_resource('http://msoe.us/CS/', 'index.html')

    # If you find fun examples of chunked or Content-Length pages, please share them with us!


def get_http_resource(url, file_name):
    """
    Get an HTTP resource from a server
           Parse the URL and call function to actually make the request.

    :param url: full URL of the resource to get
    :param file_name: name of file in which to store the retrieved resource

    (do not modify this function)
    """

    # Parse the URL into its component parts using a regular expression.
    url_match = re.search('http://([^/:]*)(:\d*)?(/.*)', url)
    url_match_groups = url_match.groups() if url_match else []
    #    print 'url_match_groups=',url_match_groups
    if len(url_match_groups) == 3:
        host_name = url_match_groups[0]
        host_port = int(url_match_groups[1][1:]) if url_match_groups[1] else 80
        host_resource = url_match_groups[2]
        print('host name = {0}, port = {1}, resource = {2}'.format(host_name, host_port, host_resource))
        status_string = make_http_request(host_name.encode(), host_port, host_resource.encode(), file_name)
        print('get_http_resource: URL="{0}", status="{1}"'.format(url, status_string))
    else:
        print('get_http_resource: URL parse failed, request not sent')


def make_http_request(host, port, resource, file_name):
    """
    Get an HTTP resource from a server

    :param bytes host: the ASCII domain name or IP address of the server machine (i.e., host) to connect to
    :param int port: port number to connect to on server host
    :param bytes resource: the ASCII path/name of resource to get. This is everything in the URL after the domain name,
           including the first /.
    :param file_name: string (str) containing name of file in which to store the retrieved resource
    :return: the status code
    :rtype: int
    :author: Vil McGann
    """
    tcp_socket = connection(host, port, resource)
    status_code = read_response_line(tcp_socket)
    byte = next_byte(tcp_socket)
    headers = get_headers(tcp_socket, byte)
    header_field_name, field_value = determine_message_type(headers)
    message = message_reader(header_field_name, field_value, tcp_socket)
    if status_code == 200:
        write_file(message, file_name)
    return status_code


def connection(host, port, resource):
    """
    Helper method for connection client to server
    :param host: The server
    :param port: Port to connect to server
    :param resource: The website of the host requested
    :return: The socket of the connection
    :author: Vil McGann
    """
    request = b'GET ' + resource + b' HTTP/1.1\r\nHost: ' + host + b'\r\n\r\n'
    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_socket.connect((host, port))
    tcp_socket.sendall(request)
    return tcp_socket


def next_byte(data_socket):
    """
        Read the next byte from the sender, received over the network.
        If the byte has not yet arrived, this method blocks (waits)
          until the byte arrives.
        If the sender is done sending and is waiting for your response, this method blocks indefinitely.

        :return: the next byte, as a bytes object with a single byte in it
    """
    return data_socket.recv(1)


def read_response_line(data_socket):
    """
    Reads the status line and returns if the request was successful (200)
    :param data_socket:
    :return: The status code as an integer
    :author: Vil McGann
    """
    status_line = []
    byte = next_byte(data_socket)
    byte_chunk = b''
    while byte != b'\x0d':
        if byte != b'\x20':
            byte_chunk += byte
        else:
            status_line.append(byte_chunk)
            byte_chunk = b''
        byte = next_byte(data_socket)
    next_byte(data_socket)  # reads the LF after the CR
    status_code = status_line[1]
    return int(status_code)


def get_headers(data_socket, byte):
    """
    Get headers from response
    :param data_socket: Connection between client and server
    :param byte: A byte from reponse.
    :return: The headers
    :author: Vil McGann
    """
    headers = []
    while byte != b'\x0d':
        headers.append(read_header(data_socket, byte))
        byte = next_byte(data_socket)
    next_byte(data_socket)  # reads the LF after the CR
    for x in range(0, len(headers)):
        print(headers[x])
    return headers


def read_header(data_socket, a_byte):
    """
    Uses the read_byte method to read the number of bytes in the following chunk then returns the value
    to the read_message method.
    :return:
    :author: Vil McGann
    """
    byte = a_byte
    header_bytes = b''
    while byte != b'\x0d':
        header_bytes += byte
        byte = next_byte(data_socket)
    next_byte(data_socket)  # reads the LF after the CR
    return header_bytes


def determine_message_type(headers):
    """
    Determine if it is a chunked message or a has a content-length
    :param headers: the headers from the response
    :return: the header's field name and value
    :author: Vil McGann
    """
    x = 0
    header_field_name = b''
    field_value = b''
    while header_field_name != b'Content-Length' and header_field_name != b'Transfer-Encoding':
        split_header = headers[x].split(b': ')
        header_field_name = split_header[0]  # header name field will always be element 0
        field_value = split_header[1]  # header value element will always be 1
        x += 1
    return header_field_name, field_value


def message_reader(header_field_name, field_value, data_socket):
    """
    Reads the message of the chunked or Content-Length response.
    :param header_field_name: Either Content-Length or Transfer-Encoding
    :param field_value: The field value of the header.
    :param data_socket: Connection between client and server
    :return: The message
    :author: Vil McGann
    """
    message = b''
    if header_field_name == b'Transfer-Encoding':
        message += read_chunks(data_socket)
    else:
        number_of_bytes = int(field_value, 16)
        message += read_non_chunked(data_socket, number_of_bytes)
    return message


def read_non_chunked(data_socket, num_of_bytes):
    """
    Reads a non-chunked body when a Content-Length header is read

    :param data_socket: data socket to read the bytes from
    :param num_of_bytes: the number of bytes in the message
    :return: the body of the HTTP response in a bytes object literal
    :author: Michael Caballero
    """
    b = b''
    for x in range(0, num_of_bytes):
        b += next_byte(data_socket)
    return b


def read_chunks(data_socket):
    """
    Reads a chunked body when a Content-Length header is not read

    :param data_socket: data socket to read from
    :return: the body of the HTTP response in a bytes object literal
    :author: Michael Caballero
    """
    b = b''
    byte = next_byte(data_socket)
    while byte != b'\x0d':
        bs = size_to_int(read_size(data_socket, byte))
        if bs != 0:
            b += read_message(bs, data_socket)
        byte = next_byte(data_socket)
    return b


def read_size(data_socket, a_byte):
    """
    Reads the size of a chunk; reads bytes until it reaches a CR LF

    :param data_socket: The data socket to receive the bytes from
    :param a_byte: The next byte
    :return: the size of a chunk as a bytes object literal
    :author: Michael Caballero
    """
    b = b''
    flag = False
    b2 = a_byte
    while not flag:
        if b2 == b'\x0d':
            next_byte(data_socket)
            flag = True
        else:
            b += b2
            b2 = next_byte(data_socket)
    return b


def size_to_int(size_byte):
    """
    Decodes the size bytes object literal into a String, removes the CR LF, and parses into integer

    :param size_byte: The size byte to parse
    :return: The size bytes object literal as an integer
    :author: Michael Caballero
    """
    i = int(size_byte, 16)
    return i


def read_message(size, data_socket):
    """
    Reads bytes equal to the size parameter

    :param size: The amount of bytes to read
    :param data_socket: The data socket to receive the bytes from
    :return: the message, as a bytes object literal
    :author: Michael Caballero
    """
    b = b''
    for x in range(0, size):
        b2 = next_byte(data_socket)
        b += b2
    next_byte(data_socket)
    next_byte(data_socket)
    return b


def write_file(payload_bytes, file_name):
    """
    Does a try-with-resources (basically) to write the message to an output file in bytes
    :param payload_bytes: message in bytes
    :param file_name: the name of the file
    :author: Vil McGann
    """
    with open(file_name, 'wb') as output_file:
        output_file.write(payload_bytes)


main()
